module condor_producer_mytoken

go 1.20

require (
	github.com/fernet/fernet-go v0.0.0-20211208181803-9f70042a33ee
	github.com/nogproject/nog v0.0.0-20191104110505-445038916a41
	github.com/oidc-mytoken/api v0.10.0
	github.com/oidc-mytoken/lib v0.7.0
)

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/hako/durafmt v0.0.0-20210608085754-5c1018a4e16b // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
