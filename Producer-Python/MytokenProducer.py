#!/usr/bin/python3

import os
import subprocess
import pwd
import time
import logging
import logging.handlers
import sys
import warnings

from optparse import OptionParser

# path where cryptography and flaat are available
# sys.path.append('/home/benoit_roland/htcondor_venv/lib/python3.6/site-packages/')
warnings.filterwarnings(action='ignore',message='Python 3.6 is no longer supported')
warnings.filterwarnings(action='ignore', category=DeprecationWarning)

from cryptography.fernet import Fernet
from flaat import Flaat

sys.tracebacklimit=0

try:
    import htcondor
except ImportError:
    htcondor = None

class MytokenProducer():

    def __init__(self):
        self.logger = None
        self.encryption_key = None

        self.oauth_issuer = 'https://login.helmholtz.de/oauth2/'
        self.oauth_issuer_name = 'helmholtz'

        self.cred_dir = None

        self.mytoken = None
        self.mytoken_file = None

        self.access_token = None
        self.access_token_file = None
        self.access_token_time = None

    # general feature
    def generate_new_key(self):
        new_key = Fernet.generate_key()
        print('new key: ' + new_key.decode('utf-8') + '\n')

    def get_encryption_key(self):
        print()
        print('### Set up Mytoken and access token credentials for user ' + pwd.getpwuid(os.getuid())[0] + ' ###')
        if (htcondor is not None) and ('SEC_ENCRYPTION_KEY_DIRECTORY' in htcondor.param):
            encryption_file = htcondor.param.get('SEC_ENCRYPTION_KEY_DIRECTORY')
            with open(encryption_file, "rb") as file:
                self.encryption_key = file.read()
            self.logger.info(' Encryption key for Fernet algorithm has been retrieved \n') 
            print()
            print('Encryption key for Fernet algorithm has been retrieved \n') 
        else:
            self.logger.error(' Could not retrieve encryption key for Fernet algorithm \n')        
            print()
            print('Could not retrieve encryption key for Fernet algorithm \n')

    def create_cred_dir(self):
        if (htcondor is not None) and ('SEC_CREDENTIAL_DIRECTORY_OAUTH' in htcondor.param):
            cred_path = htcondor.param.get('SEC_CREDENTIAL_DIRECTORY_OAUTH')
        else:
            cred_path = "/var/lib/condor/mytoken_credentials"

        try:
            self.cred_dir = cred_path + '/' + pwd.getpwuid(os.getuid())[0] + '/'

            if not os.path.isdir(self.cred_dir):
                os.makedirs(self.cred_dir)
                subprocess.run(['chmod', '0773', self.cred_dir])
                self.logger.info(' Credential directory has been created: %s \n',self.cred_dir)
            else:
                self.logger.info(' Credential directory already exists: %s \n',self.cred_dir)

        except BaseException as error:
            self.logger.error(' Could not create credential directory: %s \n', error)
            raise

    # mytoken related code 
    def create_mytoken_file(self):
        if os.path.isdir(self.cred_dir):            
            self.mytoken_file = self.cred_dir + self.oauth_issuer_name  + '.top'
            self.logger.info(' Mytoken credential file has been created: %s \n',self.mytoken_file) 
            return self.mytoken_file
        else:
            self.logger.error(' Could not create Mytoken credential file: %s \n', self.mytoken_file)
     
    def create_mytoken(self):
        try:
            mytoken_cmd = 'mytoken MT -i ' + self.oauth_issuer
            self.mytoken = subprocess.run(mytoken_cmd.split(), stdout=subprocess.PIPE).stdout.decode('ascii').strip('\n')
            self.logger.info(' Mytoken credential has been created: %s \n', self.mytoken)
            return self.mytoken
        except BaseException as error:
            self.logger.error(' Could not create Mytoken credential: %s \n', error)
            raise SystemExit(' Could not create Mytoken credential: %s \n', error)

    def encrypt_mytoken(self):
        try:
            crypto = Fernet(self.encryption_key)        
            mytoken_encrypted = crypto.encrypt(self.mytoken.encode('utf-8'))
            self.logger.info(' Mytoken credential has been encrypted: %s \n', mytoken_encrypted.decode('utf-8'))
            return mytoken_encrypted
        except BaseException as error:
            self.logger.error(' Could not encrypt Mytoken credential: %s \n', error)
            raise

    def write_mytoken(self, mytoken_encrypted):
        try:
            with open(self.mytoken_file, "wb") as file:
                file.write(mytoken_encrypted)
            self.logger.info(' Mytoken credential has been written to file: %s \n',self.mytoken_file)
            print()
            print('Mytoken credential has been written to file: ' + self.mytoken_file + '\n')
        except BaseException as error:
            self.logger.error(' Could not write Mytoken credential to file: %s \n', error)
            raise

    # access token related code  
    def create_access_token_file(self):
        if os.path.isdir(self.cred_dir):
            self.access_token_file = self.cred_dir + self.oauth_issuer_name  + '.use'
            self.logger.info(' Access token credential file has been created: %s \n',self.access_token_file)
            return self.access_token_file
        else:
            self.logger.error(' Could not create access token credential file: %s \n', self.access_token_file)

    def create_access_token(self):       
        try:
            with open(self.mytoken_file, "rb") as file:
                crypto = Fernet(self.encryption_key)
                mytoken_encrypted = file.read()
                mytoken_decrypted = crypto.decrypt(mytoken_encrypted)
                self.logger.info(' Mytoken credential has been decrypted: %s\n', mytoken_decrypted.decode('utf-8'))
            
                access_token_cmd = 'mytoken AT --MT ' + mytoken_decrypted.decode('utf-8')
                self.access_token = subprocess.run(access_token_cmd.split(), stdout=subprocess.PIPE).stdout.decode('ascii').strip('\n')
                self.logger.info(' Access token credential has been created \n')
           
        except BaseException as error:
            self.logger.error(' Could not create access token credential: %s \n', error)
            raise SystemExit(' Could not create access token credential: %s \n', error)
                    
    def write_access_token(self):
        try:
            with open(self.access_token_file, "w") as file:
                file.write(self.access_token)
            self.logger.info(' Access token credential has been written to file: %s \n',self.access_token_file)
            print('Access token credential has been written to file: ' + self.access_token_file + '\n')
        except BaseException as error:
            self.logger.error(' Could not write access token credential to file: %s \n', error)
            raise

        self.get_access_token_time()

    def signal_handler(self, logger, queue, signal_value, frame):
        self.logger.info('signal %s has been catched',signal_value)
        # use SIGHUP to trigger the reading of the credential directory
        if signal_value == signal.SIGHUP:
            self.logger.info(' SIGHUP signal has been catched: trigger reading of the credential directory \n')
            queue.put(False, block=False)
            return
        # other signals used to exit the monitoring
        queue.put(True, block=False)
        self.logger.info('Signal %s has been catched in frame %s - terminating \n', signal_value, frame)
        sys.exit(0)        

    def get_access_token_time(self):
        flaat = Flaat()
        try:
            with open(self.access_token_file, "r") as file:
                token_data = file.read()
                access_token_info = flaat.get_info_thats_in_at(token_data)
                self.logger.info(' Information retrieved from access token %s: %s \n', self.access_token_file, access_token_info)
        except BaseException as error:
            self.logger.error(' Could not retrieve access token credential information: %s \n', error)
            return True

        self.access_token_time = access_token_info['body']['exp']
        self.logger.info(' Access token expiration time: %s \n', self.access_token_time)
        self.logger.info(' Access token remaining lifetime: %s \n', self.access_token_time - time.time())

    def setup_logger(self, condor_logpath, condor_loglevel):

        # log_path
        if (htcondor is not None) and (condor_logpath in htcondor.param):
            log_path = htcondor.param.get(condor_logpath)
        else:
            print(' The variable ' + condor_logpath + ' should be defined in the HTCondor configuration \n')
            return

        # log_level
        if (htcondor is not None) and (condor_loglevel in htcondor.param):
            log_level = logging.getLevelName(htcondor.param[condor_loglevel])
        else:
            log_level = logging.INFO

        # logger
        root_logger = logging.getLogger()
        root_logger.setLevel(log_level)

        log_format = '%(asctime).19s - ' + pwd.getpwuid(os.getuid())[0] + ' - %(name)s - %(levelname)s - %(message)s'

        old_euid = os.geteuid()
        try:
            condor_euid = pwd.getpwnam("condor").pw_uid
            os.seteuid(condor_euid)
        except:
            pass

        try:
            log_handler = logging.handlers.WatchedFileHandler(log_path)
            log_handler.setFormatter(logging.Formatter(log_format))
            root_logger.addHandler(log_handler)

            # child logger
            self.logger = logging.getLogger(os.path.basename(sys.argv[0]) + '.' + self.__class__.__name__)

        finally:
            try:
                os.seteuid(old_euid)
            except:
                pass

        return self.logger



