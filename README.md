# C4P HTCondor Producer

## Description

- Script to produce refresh tokens provided by the Mytoken service and access tokens provided by the Helmholtz AAI provider.

## Usage

- ```condor_producer_mytoken```
- all parameters are read from a configuration file

## Documentation

[Mytoken documentation](https://mytoken-docs.data.kit.edu)

[Mytoken repository](https://github.com/oidc-mytoken)

[Mytoken server instance](https://mytoken.data.kit.edu)

[Mytoken library](https://pkg.go.dev/github.com/oidc-mytoken/lib)

[Mytoken library - mytoken](https://pkg.go.dev/github.com/oidc-mytoken/lib/mytoken.go)

[Mytoken library - access token](https://pkg.go.dev/github.com/oidc-mytoken/lib/accesstoken.go)

[Mytoken client](https://pkg.go.dev/github.com/oidc-mytoken/client)

[Mytoken client - MT](https://pkg.go.dev/github.com/oidc-mytoken/client/internal/commands/mt.go)

[Mytoken client - AT](https://pkg.go.dev/github.com/oidc-mytoken/client/internal/commands/at.go)

[Mytoken api](https://pkg.go.dev/github.com/oidc-mytoken/api)